from datetime import timedelta, timezone
from datetime import datetime
from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo

global mydb

conexao = pymongo.MongoClient("mongodb+srv://botfacebook:Marina2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")
mydb = conexao['eleicoes']

def capturaSeguidores():

	candidatolink = ['https://www.facebook.com/cirogomesoficial/?ref=br_rs', 'https://www.facebook.com/geraldoalckmin/', 'https://www.facebook.com/jairmessias.bolsonaro/', 'https://www.facebook.com/JoaoAmoedoNOVO/', 'https://www.facebook.com/fernandohaddad/', 'https://www.facebook.com/marinasilva.oficial/']
	candidato = ['Ciro Gomes', 'Geraldo Alckmin', 'Jair Bolsonaro', 'João Amoedo', 'Fernando Haddad', 'Marina Silva']
	contador = 0

	for y in candidatolink:

		candidatoRodada = (candidato[contador])
		Diferencahorario = timedelta(hours=-3)
		FusoHorario = timezone(Diferencahorario)
		DataHoraCapitura = datetime.now().astimezone(FusoHorario)
		curtidores = ''
		seguidores = ''

		if contador == 0:
			print(DataHoraCapitura)

		print('############ '+str(candidatoRodada)+' ############')

		seguidores = requests.get(y)
		soup = BeautifulSoup(seguidores.content, 'html.parser')

		
		for i in soup.select('div._4bl9 div'):

			if 'curtiram isso' in str(i):
				curtidores = str(i).replace('<div>','').replace('</div>','').split()[0].replace(".","")
				print('Curtidores: ' + curtidores)

			elif 'seguindo isso' in str(i):
				seguidores = str(i).replace('<div>','').replace('</div>','').split()[0].replace(".","")
				print('Seguidores: ' + seguidores + '\n')

		contador = contador + 1
		enviaDados(DataHoraCapitura, candidatoRodada, curtidores, seguidores, y)

def enviaDados(DataHora, candidato, Curtidores, Seguidores, linkPagina):

	mydb.monitoramentoPaginasFacebook.update(   
												{
													"candidato" : candidato                                       
												},
												{
													"candidato" : candidato,
													"Curtidores" : float(Curtidores),
													"Seguidores" : float(Seguidores),
													"DateTime" : DataHora,
													"URL" : linkPagina 
												},
												upsert=True,
												check_keys=True
											)
	
	mydb.historicoMonitoramentoPaginasFacebook.update	(   
															{
																"DateTime" : DataHora,
																"candidato" : candidato                                       
															},
															{
																"candidato" : candidato,
																"Curtidores" : float(Curtidores),
																"Seguidores" : float(Seguidores),
																"DateTime" : DataHora,
																"URL" : linkPagina 
															},
																upsert=True,
																check_keys=True
														)

def main():

	while True:

		capturaSeguidores()

main()
conexao.close()