from http.cookiejar import LWPCookieJar
from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import random
import time
import os

global vetor1, vetor2, s, mydb
vetor1 = []
vetor2 = []
s = requests.session()

tipo = "teste"

conexao = pymongo.MongoClient("mongodb+srv://botfacebook:Marina2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")


def buscarConta():
    mydb = conexao['contas']

    buscaMongo = mydb.facebook.find_one({ "tipo" : tipo, "status" : "livre" })

    if tipo != "teste":
        
        idConta = buscaMongo['_id']

        mydb.facebook.update_one
        (
            { 
                "_id" : idConta 
            },
            {
                "$set" : 
                            {
                                "status" : "em uso"
                            }
            }
        )

    if "email" in buscaMongo and "senha" in buscaMongo:

        return buscaMongo['email'], buscaMongo['senha'], buscaMongo['_id']

    else:

        print("Sem registro")
        conexao.close()
        quit()

def desfazerEmUso(idConta):

    mydb = conexao['contas']

    mydb.facebook.update_one
    (
        { 
            "_id" : idConta 
        },
        {
            "$set" : 
                        {
                            "status" : "livre"
                        }
        }
    )

def loginFace(login,senha):

    req = requests.get('https://m.facebook.com/')
    soup = BeautifulSoup(req.content,'html.parser')

    for x in soup.find_all('input'):
        requisicao = x['name']
        if str(requisicao) == 'lsd' or str(requisicao) == 'li' or str(requisicao) == 'm_ts':
            vetor1.append(requisicao)
            resposta = x['value']
            vetor2.append(resposta)

    dicionario = dict(zip(vetor1,vetor2))
    dicionario['email'] = login
    dicionario['pass'] = senha
    dicionario['login'] = 'Log In'

    r = s.post('https://m.facebook.com/login/?ref=dbl&fl&refid=9', data=dicionario)
    print("loguei")
    timeRandom = random.randint(8, 10)
    time.sleep(timeRandom)


def reqComentaios(linkPublicacao):
    pageComentarios = s.get('https://m.facebook.com' + str(linkPublicacao))
    capituraComentarios(pageComentarios)
    
def capituraComentarios(requisicao):

    soup = BeautifulSoup(requisicao.content, 'html.parser')
    filtro = soup.select('div.do')

    proximaPagina = soup.select("div.dm.ek a") 
    for A in proximaPagina:
        complementoLink = A['href']

    for tag1 in soup.find_all("a"):
        tag1.replaceWith("")

    for tag2 in soup.find_all("span"):
        tag2.replaceWith("")
    
    for tag3 in soup.find_all("img"):
        tag3.replaceWith("")

    for coment in filtro:
        comentario = str(coment).replace('<div class="do">','').replace('</div>','').replace("<br/>","").replace("<wbr/>","")
        print("-> " + str(comentario) + "\n")

    novosComentarios(complementoLink)

def novosComentarios(complemento):

    pageComentarios = s.get('https://m.facebook.com' + complemento)
    soup = BeautifulSoup(pageComentarios.content, 'html.parser')
    
    filtro = soup.select('div.dp')
    proximaPagina = soup.select("div.dm.dn a") 
    for A in proximaPagina:
        complementoLink = A['href']


    for tag1 in soup.find_all("a"):
        tag1.replaceWith("")

    for tag2 in soup.find_all("span"):
        tag2.replaceWith("")
    
    for tag3 in soup.find_all("img"):
        tag3.replaceWith("")

    for coment in filtro:
        comentario = str(coment).replace('<div class="dp">','').replace('</div>','').replace("<br/>","").replace("<wbr/>","")

        if not comentario:
            a = 1
        else:
            print("-> " + str(comentario) + "\n")

    if len(proximaPagina) != 0:
        novosComentarios(complementoLink)
    else:
        acabo()
        

def acabo():
    print("acabo")


def main():

    email, senha, idConta = buscarConta()
    loginFace(email,senha)
    reqComentaios("/jairmessias.bolsonaro/photos/a.250567771758883/1247169592098691/?type=3&source=48")
    acabo()

    if tipo != "teste":
        desfazerEmUso(idConta)


main()

conexao.close()