import nltk
import re
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import cross_val_predict
import pymongo

conexao = pymongo.MongoClient("mongodb+srv://bottwitter:Daciolo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")
mydb = conexao['eleicoes']

dadosProcessar = mydb.comentarios.find({ 'tipo' : 'twitter', 'polaridade' : { '$exists' : False } })

def atualizaDadosMongo(idMongo, polaridade):

    mydb.comentarios.update_one (
                                    {
                                        '_id' : idMongo
                                    },
                                    {
                                        '$set' :    {
                                                        'polaridade' : polaridade
                                                    }
                                    }
                                )

dataset = pd.read_csv('TweetsTreino.csv',encoding='utf-8')    

def Preprocessamento(instancia):
    #remove links, pontos, virgulas,ponto e virgulas dos tweets
    #coloca tudo em minusculo
    instancia = re.sub(r"http\S+", "", instancia).lower().replace(',','').replace('.','').replace(';','').replace('-','').replace(':','')
    return (instancia)

tweets = dataset['Text'].values
classes = dataset['Classificacao'].values

#Gerando Modelo

vectorizer = CountVectorizer(ngram_range=(1,2))
freq_tweets = vectorizer.fit_transform(tweets)
modelo = MultinomialNB()
modelo.fit(freq_tweets,classes)


for item in dadosProcessar:

    tweetMongo = item['comentario']

    textoProcessado = Preprocessamento(tweetMongo)
    vetorAprocessar = [textoProcessado]

    freq_testes = vectorizer.transform(vetorAprocessar)
    resultado = modelo.predict(freq_testes)

    atualizaDadosMongo(item['_id'], resultado[0])

conexao.close()