const mongoose = require("mongoose")
const Comentarios = require("../models/comentarios")

exports.comentarios = function(req, res) {
    Comentarios.find({}).limit(100).exec((err, result) => {
        if (err) {
            console.log(err)
            res.status(500).json({
                'message': err
            })
        } else {
            res.status(200).json(result)
        }
    })
}

exports.comentariosPolarizados = function(req, res) {
    Comentarios.find({ 'polaridade' : { $exists : true } }).limit(50).sort({ data : -1 }).exec((err, result) => {
        if (err) {
            res.status(500).json({
                'message' : err
            })
        } else {
            res.status(200).json(result)
        }
    })
}

exports.qtdPorCandidato = function(req, res) {

    Comentarios.aggregate([
        {
            "$unwind" : "$envolvidos"
        },
        {
            "$group" : { "_id" : "$envolvidos", "count" : { "$sum" : 1 } }
        },
        {
            "$group" : { "_id" : null, "envolvidos_detalhes" : 
            { "$push" : { "envolvidos" : "$_id", "count" : "$count" } } }
        },
        {
            "$project" : {"_id":0,"envolvidos_detalhes":1}
        }
    ], (err, result) => {
        if (err) {
            res.status(500).json({
                'message' : err
            });
        } else {
            if (!result) {
                res.status(404).json({
                    'message' : 'não encontrado'
                });
            } else {
                res.status(200).json(result[0])
            }
        }
    });
}

exports.qtdPorCandidatosBeforeSegundoTurno = function(req, res) {
    Comentarios.aggregate([
        {
            "$unwind" : "$envolvidos"
        },
        {
            "$match" : { "data" : { "$lte" : new Date('2018-10-28') }}
        },
        {
            "$group" : { "_id" : "$envolvidos", "count" : { "$sum" : 1 } }
        },
        {
            "$group" : { "_id" : null, "envolvidos_detalhes" : 
            { "$push" : { "envolvidos" : "$_id", "count" : "$count" } } }
        },
        {
            "$project" : {"_id":0,"envolvidos_detalhes":1}
        }
    ], (err, result) => {
        if (err) {
            res.status(500).json({
                'message' : err
            });
        } else {
            if (!result) {
                res.status(404).json({
                    'message' : 'não encontrado'
                });
            } else {
                res.status(200).json(result[0])
            }
        }
    });
}

exports.qtdPorCandidatosBeforePrimeiroTurno = function(req, res) {
    Comentarios.aggregate([
        {
            "$unwind" : "$envolvidos"
        },
        {
            "$match" : { "data" : { "$lte" : new Date('2018-10-07') }}
        },
        {
            "$group" : { "_id" : "$envolvidos", "count" : { "$sum" : 1 } }
        },
        {
            "$group" : { "_id" : null, "envolvidos_detalhes" : 
            { "$push" : { "envolvidos" : "$_id", "count" : "$count" } } }
        },
        {
            "$project" : {"_id":0,"envolvidos_detalhes":1}
        }
    ], (err, result) => {
        if (err) {
            res.status(500).json({
                'message' : err
            });
        } else {
            if (!result) {
                res.status(404).json({
                    'message' : 'não encontrado'
                });
            } else {
                res.status(200).json(result[0])
            }
        }
    });
}