const mongoose = require("mongoose")
const PaginaFacebook = require("../models/paginaFacebook");
const PaginaTwitter = require("../models/paginaTwitter");
const PaginaInstagram = require("../models/paginaInstagram");

exports.facebook = function (req, res, next) {
    PaginaFacebook.find({}, (err, result) => {
        if (err) {
            console.log(err)
            res.status(500).json({
                error: err
            })
        } else {
            res.status(200).json(result)
        }
    })
};

exports.twitter = function (req, res, next) {
    PaginaTwitter.find({}, (err, result) => {
        if (err) {
            console.log(err)
            res.status(500).json({
                error: err
            })
        } else {
            res.status(200).json(result)
        }
    })
};

exports.instagram = function (req, res, next) {
    PaginaInstagram.find({}, (err, result) => {
        if (err) {
            console.log(err)
            res.status(500).json({
                error: err
            })
        } else {
            res.status(200).json(result)
        }
    })
};