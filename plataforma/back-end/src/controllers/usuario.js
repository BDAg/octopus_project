var mongoose = require('mongoose');
var Usuario = require("../models/usuario");
var nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');

exports.redefinirSenha = function(req, res) {

    const token = req.body.token;
    const novaSenha = req.body.novaSenha;
    const decoded = jwt.verify(token, "TOMAR-UM-BANHO-DE-CHUVA");

    email = decoded.email;
    senha = req.body.senhaAntiga;
    senhaNova = req.body.senhaNova;

    Usuario.findOneAndUpdate({ "email" : email, "senha" : senha },{ "$set" : { "senha" : senhaNova } },(err, result) => {
        if (err) {
            // console.log(err.code)
            res.status(201).json({
                "mensagem": "Falha ao redefinir senha"
            })
        } else {
            // console.log(result)
            res.status(200).json(result)
        }
    })

};

exports.cadastrar = function(req, res) {
    const usuario = new Usuario({
        nomeCompleto: req.body.nomeCompleto,
        email: req.body.email,
        senha: req.body.senha
    })
    usuario.save((err, result) => {
        if (err) {
            // console.log(err.code)
            res.status(201).json({
                "mensagem": "Falha ao efetuar o cadastro"
            })
        } else {
            // console.log(result)
            // res.status(201).json(usuario)
            var token = jwt.sign({userID: result._id, email: req.body.email}, 'TOMAR-UM-BANHO-DE-CHUVA', {expiresIn: '2h'});
            res.status(201).json({"status" : "ok", "token" : token});
        }
    })
};

exports.login = function(req, res) {
    Usuario.find({email:req.body.email, senha:req.body.senha}, (err, result) => {
        if (err) {
            // console.log(err)
            res.status(201).json({
                "mensagem": "Erro"
            })
        } else {
            if (result[0]) {
                // console.log(result[0])
                var token = jwt.sign({userID: result._id, email: req.body.email}, 'TOMAR-UM-BANHO-DE-CHUVA', {expiresIn: '2h'});
                res.status(201).json({"status" : "ok", "token" : token, "nomeCompleto" : result[0].nomeCompleto});
            } else {
                res.status(201).json({
                    "status": "not ok"
                })            
            }
        }
    })
}

exports.buscaUsuario = function(req, res) {
    Usuario.findById({_id: req.params.id}, (err, result) => {
        if (err) {
            // console.log(err)
            res.status(500).json({
                erro: "Usuario não encontrado"
            })
        } else {
            // console.log(result);
            res.status(200).json(result)
        }
    })
};

exports.deletarUsuario = function(req, res) {
    try {
        Usuario.deleteOne({_id: objectId(req.params.id)}, (err, result) => {
            if (err) {
                // console.log(err)
                res.status(500).json({
                    error: err
                })
            } else if (result.n === 1){
                // console.log(result)
                res.status(200).json({
                    "mensagem": "ok"
                })
            } else {
                res.status(500).json({
                    "mensagem": "não encontrado"
                })
            }
        })
    } catch (error) {
        console.log("Erro")
    }
};