const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const objectId = require("mongoose").Types.ObjectId
const Usuario = require("../models/usuario");
var nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
var UsuarioController = require('../controllers/usuario');

var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'octopusprojectoficial@gmail.com', // generated ethereal user
        pass: 'lula2018' // generated ethereal password
    },
    tls: {
        rejectUnauthorized: false 
    }
});

router.post("/redefinirsenha/", UsuarioController.redefinirSenha)

router.post("/cadastrar", UsuarioController.cadastrar)

router.post("/login", UsuarioController.login)

router.get("/usuarios", (req, res, next) => {
    Usuario.find({}, (err, result) => {
        if (err) {
            console.log(err)
            res.status(500).json({
                error: err
            })
        } else {
            res.status(200).json(result)
        }
    })
});

router.get("/usuarios/:id", UsuarioController.buscaUsuario)

router.delete("/usuarios/:id", UsuarioController.deletarUsuario)

router.post("/esqueceusenha", (req, res, next) => {
    email = req.body.email;

    Usuario.findOne({
        "email": email
    }, (err, result) => {
        if (err) {
            res.status(400).json({
                "mensagem": "Não encontrado"
            })
        } else {
            if (result) {

            const token = jwt.sign({
                    email: req.body.email,
                    nome: result.nomeCompleto
                },
                "TOMAR-UM-BANHO-DE-CHUVA", {
                    expiresIn: "2 days"
                });

            conteudo_email = `Oi ${result.nomeCompleto},<br><br>
    Esqueceu sua senha? Tudo bem, não nos esquecemos de você! Clique no link a seguir para redefinir sua senha.<br><br>
    https://octopus-project.herokuapp.com/recuperar-senha?token=${token} <br><br>
    Se somente clicando não funcionar, você pode copiar e colar o link na barra de endereços do seu navegador.<br><br>
    Se você não pediu para redefinir sua senha, simplesmente ignore este e-mail.<br><br>
    `;

            var mailOptions = {
                from: 'octopusprojectoficial@gmail.com',
                to: email,
                subject: 'Recuperação de Conta Octopus Project',
                html: conteudo_email
            };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    // res.status(500).res({
                    //     "mensagem" : "deu ruim"
                    // })
                    console.log('enviado' + error);
                } else {
                    console.log('E-mail Enviado: ' + info.response);
                    // res.status(200).json({
                    //     "mensagem" : "Email enviado com sucesso"
                    // });
                }
            });

            res.status(200).json({
                "mensagem" : "Email enviado com sucesso"
            })
        } else {
            res.status(400).json({
                'mensagem' : 'e-mail não encontrado'
            });
        }
        }


    });
}
);

router.post('/recuperar_senha', (req, res, next) => {

    const token = req.body.token;
    const novaSenha = req.body.novaSenha;
    const decoded = jwt.verify(token, "TOMAR-UM-BANHO-DE-CHUVA");
    console.log(decoded);
    tokenDecodificado = decoded;
    console.log(tokenDecodificado);

    Usuario.findOneAndUpdate({ "email" : decoded.email },{ "$set" : { "senha" : novaSenha } },(err, result) => {
        if (err) {
            // console.log(err.code)
            res.status(201).json({
                "mensagem": "Falha ao redefinir senha"
            })
        } else {
            // console.log(result)
            res.status(200).json(result)
        }
    })


});

module.exports = router