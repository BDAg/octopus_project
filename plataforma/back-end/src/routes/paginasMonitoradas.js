const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const PaginasMonitoradas = require("../controllers/paginasMonitoradas");


router.get("/facebook", PaginasMonitoradas.facebook);
router.get("/twitter", PaginasMonitoradas.twitter);
router.get("/instagram", PaginasMonitoradas.instagram);

module.exports = router
