const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Comentarios = require("../controllers/comentarios");

router.get("/", Comentarios.comentarios);
router.get("/comentariospolarizados", Comentarios.comentariosPolarizados);
router.get("/qtd-por-candidatos", Comentarios.qtdPorCandidato);
router.get("/qtd-por-candidatos-before-segundo-turno", Comentarios.qtdPorCandidatosBeforeSegundoTurno);
router.get("/qtd-por-candidatos-before-primeiro-turno", Comentarios.qtdPorCandidatosBeforePrimeiroTurno);

module.exports = router;