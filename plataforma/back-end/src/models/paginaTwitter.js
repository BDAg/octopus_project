var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var monitoramentoTwitter = new Schema({
    candidato: { type: String },
    curtidas: { type: Number },
    DateTime: { type: Date },
    }, {
    collection: "monitoramentoSeguidoresTwitter"
    })

module.exports = mongoose.model("MonitoramentoPaginasTwitter", monitoramentoTwitter);