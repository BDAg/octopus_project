var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var monitoramentoFacebook = new Schema({
    candidato: { type: String},
    Curtidores: { type: Number },
    Seguidores: { type: Number },
    DateTime: { type: Date },
    URL: { type: String },
    }, {
    collection: "monitoramentoPaginasFacebook"
    })

module.exports = mongoose.model("MonitoramentoPaginasFacebook", monitoramentoFacebook);