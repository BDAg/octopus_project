var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var comentariosSchema = new Schema({
    comentario: {type: String},
    data: {type: Date},
    tipo: {type: String},
    postou: {type: String},
    polaridade: {type: String}
})

module.exports = mongoose.model("comentarios", comentariosSchema);