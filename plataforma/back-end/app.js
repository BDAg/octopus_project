const express = require("express")
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors')
const morgan = require("morgan");
const mongoose = require("mongoose");

const apiRouter = require("./src/routes/api");
const comentarios = require("./src/routes/comentarios");
const paginasMonitoradas = require("./src/routes/paginasMonitoradas");


mongoose.connect('mongodb+srv://apiOctopus:earthisflat@octopus-4fjdp.mongodb.net/eleicoes?retryWrites=true', { useNewUrlParser: true }); 
mongoose.Promise = global.Promise;

app.use(cors());

app.all(function (req, res, next) {

    var origin = req.get('origin'); 

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', origin);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
// app.use(morgan('dev'));

// Rota para criação, login de usuarios
app.use("/api", apiRouter);
// Rota para os comentarios sobre os candidatos
app.use("/comentarios", comentarios);
// Rota para os perfis monitorados nas redes sociais
app.use("/paginas", paginasMonitoradas);


app.use((req, res, next) => {
    const erro = new Error("Não encontrado");
    erro.status = 404;
    next(erro);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        erro: {
            mensagem: error.message
        }
    })
})

module.exports = app;