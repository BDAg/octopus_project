import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit {

  token: any;
  senhaAntiga: any;
  senha: any;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.token = localStorage.getItem('access_token');
  }

  redefinirSenha() {
    this.api.redefinirSenha({'token' : this.token, 'senhaAntiga' : this.senhaAntiga, 'senhaNova' : this.senha}).subscribe(
      result => {
        this.mostrarMensagemSucesso();
      },
      err => {
        this.mostrarMensagemErro();
      }
    );
  }

  mostrarMensagemSucesso() {
    const mensagem = document.getElementById('sucesso');
    mensagem.hidden = false;
  }

  mostrarMensagemErro() {
    const mensagem = document.getElementById('erro');
    mensagem.hidden = false;
  }

}
