import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../services/api.service';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})


export class CadastroComponent implements OnInit {
  // Variavel para o form
  cadastroForm: FormGroup;
  // Variavel para indicar se o form foi enviado
  submitted: boolean = false;
  // Variavel para exibir mensagem de erro
  mensagem: string;
  nomeCompleto: string;
  email: string;
  senha: string;
  formCadastro: object;

  constructor(private formBuilder: FormBuilder, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.cadastroForm = this.formBuilder.group({
      nomeCompleto: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  get f() { return this.cadastroForm.controls; }

  cadastrar() {
    this.submitted = true;
    if (this.cadastroForm.invalid) {
      return;
    } else {
      this.api.cadastrar(this.cadastroForm.value).subscribe( res => {
        console.log(res);
        if ('mensagem' in res) {
          this.mensagem = "E-mail já cadastrado";
          console.log(res['mensagem']);
        } else {
          console.log('O token e');
          console.log(res['token']);
          localStorage.setItem('isLoggedIn', 'true');
          localStorage.setItem('nome_completo', this.cadastroForm.value.nomeCompleto);
          localStorage.setItem('access_token', res['token']);
          this.router.navigate(['/painel']);
          window.location.reload();
        }
      },
      err => {
        this.mensagem = "Erro ao conectar com o servidor";
        console.log(err);
      }
      );
    }
  }


}
