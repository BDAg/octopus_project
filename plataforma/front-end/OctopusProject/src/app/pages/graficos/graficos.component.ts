import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.css']
})
export class GraficosComponent implements OnInit {

  comentariosMongo: any;
  filtro: string;
  resultado: any;
  ordenacao = 'recente';
  candidatosBarras = [];
  dadosCandidatos = [];
  candidatosBarrasBeforeSegundo = [];
  dadosCandidatosBeforeSegundo = [];
  candidatosBarrasBeforePrimeiro = [];
  dadosCandidatosBeforePrimeiro = [];

  constructor(private api: ApiService) { }

  geral = 'geral';
  bolsonaro = 'bolsonaro';
  ciro = 'ciro';
  alckmin = 'alckmin';
  amoedo = 'amoedo' ;
  marina = 'marina' ;
  haddad = 'haddad' ;

  ngOnInit() {
    this.graficoClassificacao();
    this.dadosPolarizados();
    this.graficoTwitter();
    this.graficoFacebook();
    this.graficoInstagram();
    this.barrasGeral();
    this.barrasBeforeSegundoTurno();
    this.barrasBeforePrimeiroTurno();
  }

  graficoFacebook() {
    this.api.seguidoresFacebook().subscribe(
      result => {
        this.candidatos(result, 'graficoFacebook');
      }, err => {
        console.log(err);
      }
    );
  }

  graficoTwitter() {
    this.api.seguidoresTwitter().subscribe(
      result => {
        this.candidatos(result, 'graficoTwitter');
      }, err => {
        console.log(err);
      }
    );
  }

  graficoInstagram() {
    this.api.seguidoresInstagram().subscribe(
      result => {
        this.candidatos(result, 'graficoInstagram');
      }, err => {
        console.log(err);
      }
    );
  }


  graficoClassificacao() {
    // Armazenando os dados do banco
    this.api.comentarios().subscribe(
      result => {
        // Função para gerar o gráfico
        this.gerarGrafico(result);
        // console.log(result.length)
      }, err => {
        console.log(err);
      }
    );
  }

  getResultado() {
    if (this.ordenacao === '3') {
      return this.comentariosMongo.sort(function(a, b) {
        const dataA: any = new Date(a.data);
        const dataB: any = new Date(b.data);
        return dataB - dataA;
      });
    } else {
      return this.comentariosMongo;
    }
  }

  alterarOrdenacao() {
    this.resultado = this.getResultado();
    console.log(this.resultado);
  }

  dadosPolarizados() {
    this.api.comentariosPolarizados().subscribe(
      result => {
        this.comentariosMongo = result;
        this.resultado = result;
      }, err => {
        console.log(err);
      }
    );
  }

  gerarGrafico(comentarios: any) {
    const polaridade = {
      positivo: 0,
      negativo: 0,
      neutro: 0
    };

    for (let i = 0; i < comentarios.length; i++) {
      if (comentarios[i].polaridade === 'Neutro') {
        polaridade.neutro += 1;
      } else if (comentarios[i].polaridade === 'Negativo') {
        polaridade.negativo += 1;
      } else if (comentarios[i].polaridade === 'Positivo') {
        polaridade.positivo += 1;
      }
    }

    // tslint:disable-next-line:no-unused-expression
    new Chart(document.getElementById('classificacao'), {
      type: 'pie',
      data: {
        labels: ['Positivo', 'Negativo', 'Neutro'],
        datasets: [{
          label: 'Popularidade',
          backgroundColor: ['#eb3651', '#8e5ea2', '#3cba9f'],
          // Numeros aleatorios
          data: [polaridade.positivo, polaridade.negativo, polaridade.neutro]
        }]
      },
      options: {
          responsive: true
      }
    });
  }

  barrasGeral() {

    this.api.qtdPorCandidato().subscribe(
      result => {
        let resultadoAPI: any;
        resultadoAPI = result;

        for (let i = 0; i < resultadoAPI['envolvidos_detalhes'].length; i++) {
          this.candidatosBarras.push(resultadoAPI['envolvidos_detalhes'][i].envolvidos);

          if (resultadoAPI['envolvidos_detalhes'][i].count) {
            this.dadosCandidatos.push(resultadoAPI['envolvidos_detalhes'][i].count);
          }
        }
          // tslint:disable-next-line:no-unused-expression
        new Chart(document.getElementById('myBar'), {
          type: 'bar',
          data: {
            labels: this.candidatosBarras,
            datasets: [
              {
                label: 'Popularidade',
                backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                data: this.dadosCandidatos
              }
            ]
          },
          options: {
            legend: { display: false }
            // title: {
            //   display: true,
            //   text: 'Popularidade no Twitter'
            // }
          }
        });

      }, err => {
        console.log(err);
      }
    );
  }

  barrasBeforeSegundoTurno() {

    this.api.qtdPorCandidatoBeforeSegundoTurno().subscribe(
      result => {
        let resultadoAPI: any;
        resultadoAPI = result;

        for (let i = 0; i < resultadoAPI['envolvidos_detalhes'].length; i++) {
          this.candidatosBarrasBeforeSegundo.push(resultadoAPI['envolvidos_detalhes'][i].envolvidos);

          if (resultadoAPI['envolvidos_detalhes'][i].count) {
            this.dadosCandidatosBeforeSegundo.push(resultadoAPI['envolvidos_detalhes'][i].count);
          }
        }
          // tslint:disable-next-line:no-unused-expression
        new Chart(document.getElementById('myBarBeforeSegundoTurno'), {
          type: 'bar',
          data: {
            labels: this.candidatosBarrasBeforeSegundo,
            datasets: [
              {
                label: 'Popularidade',
                backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                data: this.dadosCandidatosBeforeSegundo
              }
            ]
          },
          options: {
            legend: { display: false }
            // title: {
            //   display: true,
            //   text: 'Popularidade no Twitter'
            // }
          }
        });

      }, err => {
        console.log(err);
      }
    );
  }

  barrasBeforePrimeiroTurno() {

    this.api.qtdPorCandidatoBeforePrimeiroTurno().subscribe(
      result => {
        let resultadoAPI: any;
        resultadoAPI = result;

        for (let i = 0; i < resultadoAPI['envolvidos_detalhes'].length; i++) {
          this.candidatosBarrasBeforePrimeiro.push(resultadoAPI['envolvidos_detalhes'][i].envolvidos);

          if (resultadoAPI['envolvidos_detalhes'][i].count) {
            this.dadosCandidatosBeforePrimeiro.push(resultadoAPI['envolvidos_detalhes'][i].count);
          }
        }
          // tslint:disable-next-line:no-unused-expression
        new Chart(document.getElementById('myBarBeforePrimeiroTurno'), {
          type: 'bar',
          data: {
            labels: this.candidatosBarrasBeforePrimeiro,
            datasets: [
              {
                label: 'Popularidade',
                backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                data: this.dadosCandidatosBeforePrimeiro
              }
            ]
          },
          options: {
            legend: { display: false }
            // title: {
            //   display: true,
            //   text: 'Popularidade no Twitter'
            // }
          }
        });

      }, err => {
        console.log(err);
      }
    );
  }

  gerarGraficoPizza(candidatos: any, curtidas: any, element: any) {
    // Implementar com os dados da API
    // tslint:disable-next-line:no-unused-expression
    new Chart(document.getElementById(element), {
        type: 'pie',
        data: {
          labels: candidatos,
          datasets: [{
            label: 'Popularidade',
            backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
            // Numeros aleatorios
            data: curtidas
          }]
        },
        options: {
            responsive: true
        }
    });
  }

  candidatos(result, element) {
    const candidatos = [];
    const curtidas = [];
    for (let i = 0; i < result.length; i++) {
      candidatos.push(result[i].candidato);
      if (result[i].Curtidores) {
        curtidas.push(result[i].Curtidores);
      } else if (result[i].curtidas) {
        curtidas.push(result[i].curtidas);
      } else {
        curtidas.push(result[i].seguidores);
      }
    }
    this.gerarGraficoPizza(candidatos, curtidas, element);
  }
}
