import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-graficos-resultado',
  templateUrl: './graficos-resultado.component.html',
  styleUrls: ['./graficos-resultado.component.css']
})
export class GraficosResultadoComponent implements OnInit, OnChanges {

  @Input() comentariosMongo: any;
  @Input() tipoDado: string;

  resultados: any;

  resultadoFiltrado = [];

  constructor() { }

  ngOnInit() {
    this.filtrarPorTipo();
  }

  ngOnChanges() {
    this.filtrarPorTipo();
  }

  formatarData(value: any) {

    value = new Date(value).toLocaleString();

    return value;

  }

  resultadoGeral() {
    return this.comentariosMongo;
  }

  resultadoFiltrar(stringBusca: string) {
    this.resultadoFiltrado = [];
    if (this.comentariosMongo) {
      for (let i = 0; i < this.comentariosMongo.length; i++) {
        const doc = this.comentariosMongo[i];
        if ('length' in doc.envolvidos) {
          if (doc.envolvidos.length !== 0) {
            if (doc.envolvidos.includes(stringBusca) === true) {
              this.resultadoFiltrado.push(doc);
            }
          }
        }
      }
      return this.resultadoFiltrado;
    } else {
      return [];
    }
  }

  filtrarPorTipo() {
    switch (this.tipoDado) {
      case 'geral':
        this.resultados = this.resultadoGeral();
        break;
      case 'bolsonaro':
        this.resultados = this.resultadoFiltrar('bolsonaro');
        break;
      case 'ciro':
        this.resultados = this.resultadoFiltrar('ciro');
        break;
      case 'alckmin':
        this.resultados = this.resultadoFiltrar('alckmin');
        break;
      case 'amoedo':
        this.resultados = this.resultadoFiltrar('amoedo');
        break;
      case 'marina':
        this.resultados = this.resultadoFiltrar('marina');
        break;
      case 'haddad':
        this.resultados = this.resultadoFiltrar('haddad');
        break;
    }
  }

}
