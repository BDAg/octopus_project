import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficosResultadoComponent } from './graficos-resultado.component';

describe('GraficosResultadoComponent', () => {
  let component: GraficosResultadoComponent;
  let fixture: ComponentFixture<GraficosResultadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficosResultadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficosResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
