import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-recuperar-senha',
  templateUrl: './recuperar-senha.component.html',
  styleUrls: ['./recuperar-senha.component.css']
})
export class RecuperarSenhaComponent implements OnInit {

  token: string;
  senha = '';
  senha1 = '';

  constructor(private router: Router ,private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      console.log(this.token);
  });
  }

  recuperarSenha() {
    this.api.recuperarSenha(this.token, this.senha).subscribe(res => {
      console.log(res);
      this.router.navigate(['/home']);
      window.location.reload();
    },
    err => {
      console.log(err);
    }
  );
  }

}
