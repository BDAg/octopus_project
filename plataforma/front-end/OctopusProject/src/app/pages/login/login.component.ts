import { ApiService } from './../../services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthService } from '../../auth.service';
import { first, timeout } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // Variavel para o form
  loginForm: FormGroup;
  // Variavel para indicar se o form foi enviado
  submitted = false;

  message: string;
  email: string;
  senha: string;
  status: any;
  emailRecuperacao: string;
  returnUrl = '/painel';

  constructor(private formBuilder: FormBuilder, private router: Router, public authService: AuthService, private api: ApiService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(5)]]
    });
    this.returnUrl = '/dashboard';
    this.authService.logout();
  }
  get f() { return this.loginForm.controls; }

  login() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.api.logar(this.loginForm.value).subscribe(
        result => {
          this.status = result;
          console.log(this.status.status);
          if (this.status.status === 'ok') {
            localStorage.setItem('access_token', this.status.token);
            localStorage.setItem('nome_completo', this.status.nomeCompleto);
            this.router.navigate(['/painel']);
            window.location.reload();
          } else {
            this.message = 'Login inválido';
          }
        },
        err => {
          this.message = 'Falha ao conectar com o servidor';
        }
      );
    }
  }

  mostrarEsqueceuSenha() {
    const esqueceu = document.getElementById('esqueceu');
    const login = document.getElementById('login');
    login.hidden = true;
    esqueceu.hidden = false;
  }

  mostrarMensagemSucesso() {
    const mensagem = document.getElementById('sucesso');
    mensagem.hidden = false;
  }

  mostrarMensagemErro() {
    const mensagem = document.getElementById('erro');
    mensagem.hidden = false;
  }

  resetarMensagemErro() {
    const mensagem = document.getElementById('erro');
    mensagem.hidden = true;
  }

  esqueceuSenha() {
    this.api.esqueceuSenha(this.emailRecuperacao).subscribe(
      resul => {
        if ('mensagem' in resul) {
          if (resul['mensagem'] === 'Email enviado com sucesso') {
            // A mensagem aparece mas não da tempo do usuario ver
            this.mostrarMensagemSucesso();
            this.router.navigate(['/home']);
            window.location.reload();
          }
        }
      }, err => {
        console.log(err);
        this.mostrarMensagemErro();
      }
    );
  }

}
