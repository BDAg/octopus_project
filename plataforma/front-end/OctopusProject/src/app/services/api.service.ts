import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  URL_API = 'http://api-octopus-project.herokuapp.com';
  // URL_API = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  logar(pessoa) {
    return this.http.post(`${this.URL_API}/api/login`, pessoa);
  }

  cadastrar(pessoa) {
    return this.http.post(`${this.URL_API}/api/cadastrar`, pessoa);
  }

  redefinirSenha(dados) {
    return this.http.post(`${this.URL_API}/api/redefinirsenha`, dados);
  }

  recuperarSenha(token, novaSenha) {
    return this.http.post(`${this.URL_API}/api/recuperar_senha`, {'token': token, 'novaSenha': novaSenha});
  }

  esqueceuSenha(email) {

    return this.http.post(`${this.URL_API}/api/esqueceusenha`, {'email' : email});
  }

  comentarios() {
    return this.http.get(`${this.URL_API}/comentarios`);
  }

  comentariosPolarizados() {
    return this.http.get(`${this.URL_API}/comentarios/comentariospolarizados`);
  }

  seguidoresFacebook() {
    return this.http.get(`${this.URL_API}/paginas/facebook`);
  }

  seguidoresTwitter() {
    return this.http.get(`${this.URL_API}/paginas/twitter`);
  }

  seguidoresInstagram() {
    return this.http.get(`${this.URL_API}/paginas/instagram`);
  }

  qtdPorCandidato() {
    return this.http.get(`${this.URL_API}/comentarios/qtd-por-candidatos`);
  }

  qtdPorCandidatoBeforeSegundoTurno() {
    return this.http.get(`${this.URL_API}/comentarios/qtd-por-candidatos-before-segundo-turno`);
  }

  qtdPorCandidatoBeforePrimeiroTurno() {
    return this.http.get(`${this.URL_API}/comentarios/qtd-por-candidatos-before-primeiro-turno`);
  }

}
