import pymongo
from pymongo import MongoClient
import json
conexao = pymongo.MongoClient("mongodb+srv://bottwitter:Daciolo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")
mydb = conexao['eleicoes']

bolsonaro = ['jairbolsonaro', 'bozonaro', '#elenão', '#elesim', '#BolsonaroNãoÉCorrupto', '#Bolsomito', '#Bolsonaro17', 'Bolsonaro', 'Jair se acostumando', 'Bolsomito', 'PSL']
marina = ['MarinaSilva', '#DiaDeMarina', 'Marina Silva', 'Rede 18']
haddad = ['Haddad_Fernandojoaoamoedonovo', '#ApoieHaddadManu', '#Vote13', '#LulaLivre', 'Haddad', 'Lula', 'PT']
ciro = ['cirogomes', '#Ciro12', 'Ciro', 'PDT', 'Katia Abreu']
amoedo = ['joaoamoedonovo', '#VemComJoão30', 'Amoedo', 'Partido Novo']
alckmin = ['#Geraldo45', 'Alckmin', 'PSDB']

dicionario = dict({'bolsonaro' : bolsonaro, 'marina' : marina, 'haddad' : haddad, 'ciro' : ciro, 'amoedo' :amoedo, 'alckmin' : alckmin })

tweetsMongo = mydb.comentarios.find({'tipo':'twitter', 'envolvidos' : { '$exists' : False } })

def updateDados(idMongo, vetorPoliticos):

    mydb.comentarios.update_one (
                                    {
                                        "_id" : idMongo
                                    },
                                    {
                                        '$set' :    {
                                                        'envolvidos' : vetorPoliticos
                                                    }
                                    }
                                )

for doc in tweetsMongo:
    
    computar = dict ({
                        'neutro'    : 0,
                        'bolsonaro' : 0,
                        'marina'    : 0,
                        'haddad'    : 0,
                        'ciro'      : 0,
                        'amoedo'    : 0,
                        'alckmin'   : 0
                    })
    
    tweet = doc['comentario']
    for indice in dicionario.keys():
        for i in dicionario[indice]:
            if str(i).lower() in str(tweet).lower():
                qtd = computar[indice]
                computar[indice] = qtd+1
    
    nomesRegistrados = [campo for campo in computar.keys() if computar[campo] != 0]
    
    updateDados( doc['_id'], nomesRegistrados )

conexao.close()