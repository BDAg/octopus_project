# Documentação

## Sobre o Projeto

O Projeto tem como objetivo monitorar os usuários das redes sociais Facebook, Twitter e Instagram com ênfase nas postagens com teor político a partir de WebBot e Machine Learning.

Para saber mais [clique aqui.](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/home)

Para acessar o cronograma [clique aqui.](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/cronograma)

Para seguir o tutorial de instalação [clique aqui.](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Documentação-da-Solução#montagem-do-ambiente-do-desenvolvimento)

## Andamento do Projeto

- [x] [Definições de Projeto](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/home)
- [x] [Documentação da Solução](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Documentação-da-Solução)
- [x] Desenvolvimento da Solução (Sprint 1)
- [x] Desenvolvimento da Solução (Sprint 2)
- [x] Desenvolvimento da Solução (Sprint 3)
- [x] Desenvolvimento da Solução (Sprint 4)
- [x] Fechamento do Projeto

## Equipe

- [Iam Caio (CTO)](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Iam-Caio)

- [José Pedro (CTO Aprendiz)](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/José-Pedro)

- [José Maran](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/José-Maran)

- [Davi Dutra](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Davi)

- [Dério Lucas](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Derio-Lucas)

- [Matheus Justino](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Matheus-Justino)

- [Gustavo Andrade](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Gustavo-Rodrigues)

- [Pedro Henrique](https://gitlab.com/BDAg/octopus/acompanhamento-eleicoes/wikis/Pedro)
