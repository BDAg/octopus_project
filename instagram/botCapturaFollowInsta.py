from bs4 import BeautifulSoup
import json, random, re, requests, time
from datetime import datetime
from datetime import timedelta, timezone
import pymongo

global USERNAME, PASSWD, session, mydb

conexao = pymongo.MongoClient("mongodb+srv://botinstagram:Amoedo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")

mydb = conexao['contas']

loginMongo = mydb.instagram.find_one({'status':'livre','tipo':'mineracao'})

USERNAME = loginMongo['email']
PASSWD = loginMongo['senha']

conexao.close()

urls = [ "https://www.instagram.com/jairmessiasbolsonaro/", "https://www.instagram.com/cirogomes/?hl=pt-br", "https://www.instagram.com/geraldoalckmin_/?hl=pt-br" , "https://www.instagram.com/joaoamoedonovo/?hl=pt-br" , "https://www.instagram.com/fernandohaddadoficial/?hl=pt-br" , "https://www.instagram.com/_marinasilva_/?hl=pt-br" ]

nomes = [ "jair bolsonaro", "Ciro Gomes", "Geraldo Alckmin", "João Amoedo", "Fernando Haddad", "Marina Silva"]

session = requests.Session()

def enviaDados(datahora, seguidores, nomes):
	mydb.monitoramentoSeguidoresInstagram.update( 	
													{
														"candidato" : nomes,
													},
													{
														"candidato" : nomes,
														"seguidores" : seguidores,
														"datetime" : datahora
													},
													upsert=True,
													check_keys=True
												)

	mydb.historicoMonitoramentoSeguidoresInstagram.update( 	
													{
														"datetime" : datahora,
														"candidato" : nomes,
													},
													{
														"candidato" : nomes,
														"seguidores" : seguidores,
														"datetime" : datahora
													},
													upsert=True,
													check_keys=True
												)

def login():
	BASE_URL = 'https://www.instagram.com/accounts/login/'
	LOGIN_URL = BASE_URL + 'ajax/'

	headers_list =  [
				"Mozilla/5.0 (Windows NT 5.1; rv:41.0) Gecko/20100101"\
				" Firefox/41.0",
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2)"\
				" AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2"\
				" Safari/601.3.9",
				"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0)"\
				" Gecko/20100101 Firefox/15.0.1",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36"\
				" (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"\
				" Edge/12.246"
			]

	USER_AGENT = headers_list[random.randrange(0,4)]

	session.headers = {'user-agent': USER_AGENT}
	session.headers.update({'Referer': BASE_URL})    
	req = session.get(BASE_URL)    
	soup = BeautifulSoup(req.content, 'html.parser')    
	body = soup.find('body')

	pattern = re.compile('window._sharedData')
	script = body.find("script", text=pattern)

	script = script.get_text().replace('window._sharedData = ', '')[:-1]
	data = json.loads(script)

	csrf = data['config'].get('csrf_token')
	login_data = {'username': USERNAME, 'password': PASSWD}
	session.headers.update({'X-CSRFToken': csrf})
	session.post(LOGIN_URL, data=login_data, allow_redirects=True)

def redirecionarPagina(url):

	htmlHome = session.get(url)

	htmlPront = str(htmlHome.text)

	seguidores = mineracaoFollows(htmlPront)

	return seguidores

def mineracaoFollows(htmlParseado):

	contador = htmlParseado.find('"edge_followed_by":') #procurando este texto

	textoPreparacao = htmlParseado[contador+19:] #acha a posição de inicio do caracter "edge..."

	textoPreparacao = textoPreparacao.split(',',1) #quebra o código na primeira virgula

	numeroDesejado = json.loads(str(textoPreparacao[0]))
	
	seguidores = numeroDesejado['count']

	return seguidores
	

def esperaRandomica():

	tempoRandomico = random.randint(8,20)
	time.sleep(tempoRandomico)

def main():

	login()
	# esperaRandomica()
	while True:

		conexao = pymongo.MongoClient("mongodb+srv://botinstagram:Amoedo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")
		mydb = conexao['eleicoes']

		for url, nome in zip(urls, nomes):
			diferencaHorario = timedelta(hours = -3)
			fusoHorario = timezone(diferencaHorario)
			datahora = datetime.now()
			print(nome, datahora)
			seguidores = redirecionarPagina(url)
			enviaDados(datahora, seguidores, nome)
			print(seguidores)
			time.sleep(10)

		conexao.close()


main()
