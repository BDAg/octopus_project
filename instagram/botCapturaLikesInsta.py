from bs4 import BeautifulSoup
import json, random, re, requests, time
import pymongo

global USERNAME, PASSWD, session

conexao = pymongo.MongoClient('mongodb+srv://botinstagram:Amoedo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true')
mydb = conexao['eleicoes']

loginMongo = mydb.instagram.find_one({'status':'livre','tipo':'mineracao'})

USERNAME = loginMongo['email']
PASSWD = loginMongo['senha']

def login():
	BASE_URL = 'https://www.instagram.com/accounts/login/'
	LOGIN_URL = BASE_URL + 'ajax/'

	headers_list =  [
				"Mozilla/5.0 (Windows NT 5.1; rv:41.0) Gecko/20100101"\
				" Firefox/41.0",
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2)"\
				" AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2"\
				" Safari/601.3.9",
				"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0)"\
				" Gecko/20100101 Firefox/15.0.1",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36"\
				" (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"\
				" Edge/12.246"
			]

	USER_AGENT = headers_list[random.randrange(0,4)]

	session.headers = {'user-agent': USER_AGENT}
	session.headers.update({'Referer': BASE_URL})    
	req = session.get(BASE_URL)    
	soup = BeautifulSoup(req.content, 'html.parser')    
	body = soup.find('body')

	pattern = re.compile('window._sharedData')
	script = body.find("script", text=pattern)

	script = script.get_text().replace('window._sharedData = ', '')[:-1]
	data = json.loads(script)

	csrf = data['config'].get('csrf_token')
	login_data = {'username': USERNAME, 'password': PASSWD}
	session.headers.update({'X-CSRFToken': csrf})
	login = session.post(LOGIN_URL, data=login_data, allow_redirects=True)

def redirecionarPagina():

	htmlHome = session.get("https://www.instagram.com/jairmessiasbolsonaro/")

	htmlPront = str(htmlHome.text)

	mineracaoLikes(htmlPront)

def mineracaoLikes(htmlParseado):

	contador = htmlParseado.find('"edge_owner_to_timeline_media"')

	contadorfinal = htmlParseado.find('"edge_saved_media"')

	textoCortado = str(htmlParseado[ contador : contadorfinal])[::-1].split(',', 1)[1][::-1]
	
	removendoCampo = textoCortado.split(':',1)[1]
	
	numeroDesejado = json.loads(str(removendoCampo))

	for item in numeroDesejado['edges']:

		print(item['node']['edge_liked_by']['count'])

def esperaRandomica():

	tempoRandomico = random.randint(8,20)
	time.sleep(tempoRandomico)

def main():

	login()
	# esperaRandomica()
	redirecionarPagina()

main()